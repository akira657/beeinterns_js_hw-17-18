function createSession() {
    const xhr = new XMLHttpRequest();

    xhr.open('POST','https://reqres.in/api/login');

    xhr.setRequestHeader('Content-Type','application/json; charset=utf-8');

    const data = {
    "username": "george.bluth@reqres.in",
    "email": "george.bluth@reqres.in",
    "password": "george"
    };

    xhr.send(JSON.stringify(data));

    xhr.responseType = 'json';

    xhr.onloadend = function() {
        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            fetchEmails(xhr.response.token);
        }
    };
}

function fetchEmails(token) {
    const xhr = new XMLHttpRequest();

    xhr.open('GET','https://reqres.in/api/users?per_page=10');

    xhr.setRequestHeader('Cache-Control', 'no-cache');
    xhr.setRequestHeader('Accept-Language', 'ru');
    xhr.setRequestHeader('Content-Type','application/json; charset=utf-8');
    xhr.setRequestHeader('Authorization', token);

    xhr.send();

    xhr.responseType = 'json';

    xhr.onreadystatechange = function () {
        console.log(xhr.readyState);

        if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            const arrayData = xhr.response.data;

            const emails = arrayData.map(obj => obj.email);
            console.log(emails);
        }
    };
}

createSession()